package pl.sda.chat;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/message")
public class MessageController {

	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private MessageRemoteService messageRemoteService;
	
	@GetMapping("/{senderId}/{receiverId}/{body}")
	public Message createMessage(@PathVariable String senderId,
			@PathVariable String receiverId,
			@PathVariable String body){
		
		Message message = new Message();
		message.setSenderId(senderId);
		message.setReceiverId(receiverId);
		message.setBody(body);
		message.setCreated(new Date());
		
		messageService.create(message);
		return message;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Message createMessage(@RequestBody Message message){
		message.setCreated(new Date());
		
		Message remoteMessage = messageRemoteService.createMessage(message);
		message.setMessageId(remoteMessage.getMessageId());
		messageService.create(message);
		
		return message;
	}
	
	
	@GetMapping("/")
	public List<Message> getAllMessages(){
//		return messageService.getAllMessages();
		return messageRemoteService.getMessages();
	}
	
	
	
	
}
