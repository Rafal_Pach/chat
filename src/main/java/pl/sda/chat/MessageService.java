package pl.sda.chat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.sda.user.UserRepository;
import pl.sda.user.UserService;

@Service
@Transactional
public class MessageService {
	
	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private UserService userService;
	
	
	public void create(Message message) {
		userService.createIfDoesntExist(message.getSenderId());
		userService.createIfDoesntExist(message.getReceiverId());
		messageRepository.create(message);
	}
	
	public List<Message> getAllMessages(){
		return messageRepository.getAllMessages();
	}
	

}
