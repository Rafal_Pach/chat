package pl.sda.chat;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import pl.sda.user.AbstractRemoteService;

@Service
public class MessageRemoteService extends AbstractRemoteService{
	
	public static final String RESOURCE_URL = "/api/message";
	
	public Message createMessage(Message message){
		HttpHeaders headers = getDefaultHeaders();
        RequestEntity<Message> request = new RequestEntity<>(message, headers, HttpMethod.POST, prepareUrl(RESOURCE_URL));
        return restTemplate.exchange(request, Message.class).getBody();
	}
	
	public List<Message> getMessages(){
		HttpHeaders headers = getDefaultHeaders();
        RequestEntity<Message> request = new RequestEntity<>(headers, HttpMethod.GET, prepareUrl(RESOURCE_URL));
		
        List<Message> result = restTemplate.exchange(request, new ParameterizedTypeReference<List<Message>>() {}).getBody();
        
        return result;
	}

}
