package pl.sda.user;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import pl.sda.chat.Message;

@Repository
public class UserRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<User> getUserByName(String name){
		return em.createNativeQuery("SELECT * FROM USERS where USER_ID = ?", User.class)
				.setParameter(1, name)
				.getResultList();
	}
	
	public void create(User user){
		em.persist(user);	
	}
	
	public List<User> getAllUsers(){
		return em.createNativeQuery("SELECT * FROM USERS", User.class)
				.getResultList();
	}
}
