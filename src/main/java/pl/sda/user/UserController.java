package pl.sda.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserRemoteService userRemoteservice;
	
//	@GetMapping("/api/users")
//	public List<User> showAll(){
//		return userService.showAllUsers();	
//	}
	
	@GetMapping("/api/users")
	public List<String> showAllFromServer(){
		return userRemoteservice.getAvailableContacts();
	}
	
	@GetMapping("/username")
	public String getUserName(){
		return userRemoteservice.getUserName();
	}
	
	
	

}
