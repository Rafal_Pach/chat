package pl.sda.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	public void createIfDoesntExist(String name) {
		if(userRepository.getUserByName(name).isEmpty()){
			User u = new User();
			u.setUserId(name);
			userRepository.create(u);
		}
	}
	
	public List<User> showAllUsers(){
		return userRepository.getAllUsers();
	}
}
