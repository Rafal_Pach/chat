function displayAllMessages(){
    $.get("/message/", function(data){
        for(var i=0;i<data.length;i++){
            appendMessage(data[i]);
        }
    });
     
}

function refresh(){
	$.get("/message/", function(data){
    	for(var i=0;i<data.length;i++){
        	appendMessage(data[i]);
    	}
		setTimeout(refresh, 2000);
	});
}


function submit(){
	$('#submit').click(createMessage);
}

function createMessage(){
	var senderId2 = $('.senderId').val();
	var receiverId2 = $('.receiverId').val();
	var message2 = $('.message').val();
	$.post("/message/", JSON.stringify({senderId: senderId2, receiverId: receiverId2, body: message2}), function(data){
		console.log("wiadomosc dodano")
		});
}

 
$(function(){
	$.ajaxSetup({contentType: 'application/json'});
	$.get("/username", function(data){
			$('#user').html(data)
			});
    displayAllMessages();
    submit();
    refresh();
    
});
 
function appendMessage(message){
        $('.list-group').append('<li class="list-group-item list-group-item-info">'
            + '[' + message.senderId + '] '
            + message.body
            + '<div style="float:right">' + formatDate(message.created)
            + '</div></li>');
}
 
function formatDate(epochDate){
    return new Date(epochDate);
}